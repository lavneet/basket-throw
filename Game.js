//https://jsfiddle.net/jn5ma313/49/
// 169, 31


var rad = function(angle){
	return angle*(Math.PI/180)
}

var exp = function(base, exponent){
    return parseFloat(Math.pow(base, exponent));
}

var angle = 0;
var Vr = 0;
var g = 9.8;
var Vy = 0;
var Vx = 0;
var Dyf;
var Dy;
var Dx;
var totalDx = 0;
var changeDy;
var t = 0;
var totalT = 0;

var balltop = 0;
var ballleft = 0;
var targettop = 0;
var targetbottom = 0;
var targetleft = 0;
var targetright = 0;

var easy = false;
var medium = false;
var hard = false;

var touched = false;
var balldraggable= false;

var locked = false;

var getcss = function(element, top){
    return parseInt($(element).css(top));
}

var triggerball = function(){

	initialballtop = getcss('#ball', 'top')
	initialballleft = getcss('#ball', 'left')
	//$('#info').append('<p>Total Horizontal Distance: '+totalDx.toFixed(2)+'</p>')
    var release = setInterval(function(){
        t += 1;
        Dyf = (Vy*t)-(g*0.5*(exp(t, 2)));
        Dy = (Vy*(t-1))-(g*0.5*(exp(t-1, 2)));
        Dx = Vx*t;
		changeDy = Dyf - Dy;
		$('#ball').animate({top: initialballtop - Dyf, left: initialballleft + Dx}, 300, 'linear');
		//$('body').append('<div class="dot" style="top:'+getcss('#ball', 'top')+'px; left:'+getcss('#ball', 'left')+'px;"></div>');
		if ((balltop >= targettop) && (balltop <= targetbottom) && (ballleft >= targetleft) && (balltop <= targetright)){
        }
		var validatetouch = setInterval(function(){
			targettop = getcss('#target', 'top')
			targetbottom = getcss('#target', 'top') + getcss('#target', 'height');
			targetleft = getcss('#target', 'left');
			targetright = getcss('#target', 'left') + getcss('#target', 'width');
			balltop = getcss('#ball', 'top')
			ballleft = getcss('#ball', 'left')
			if ((balltop >= targettop) && (balltop <= targetbottom) && (ballleft >= targetleft) && (ballleft <= targetright)){
				touched = true;
				$('#target').hide('explode', {pieces: 70}, 1000)
				//$('#info').append('<p>'+balltop +' >= '+targettop +'; '+balltop +' <= '+targetbottom +'; '+ballleft +' >= '+targetleft +'; '+balltop +' <= '+targetleft+'</p>')
			}
		}, 1)
		if (t > totalT) {
			t = 0;
			clearInterval(release);
			if (touched){
				$('#resultsinfo').append('<h1 style="color: #027a90">Goal reached</h1>')
				if (easy){
					$('#resultsinfo').append('<p>Your score: 500</p><button onclick="javascript: location.reload()">Reset</button>');
				}else if(medium){
					$('#resultsinfo').append('<p>Your score: 1000</p><button onclick="javascript: location.reload()">Reset</button>');
				}else if(hard){
					$('#resultsinfo').append('<p>Your score: 1500</p><button onclick="javascript: location.reload()">Reset</button>');
				}
			}else{$('#resultsinfo').append('<h1 style="color:#a0252a">Missed</h1><button onclick="javascript: location.reload()">Try again</button>')}
			$('#resultsinfo').css('display','block').stop().animate({width: '+=10px', height: '+=10px', opacity: 1}, 200);

        }
        //$('body').append('<div style="position: absolute; font-size: 10px;top:'+getcss('#ball', 'top')+'px; left:'+getcss('#ball', 'left')+'px;">'+getcss('#ball', 'left')+', '+getcss('#ball', 'top')+', '+t+'</div>');
    }, 300);
}

var targetmove = function(){
	if (easy){
		$('#target').animate({top: 600}, 2000, 'linear', function(){$(this).animate({top: 50}, 2000, 'linear')})
	}else if (medium){
		$('#target').animate({top: 600}, 1500, 'linear', function(){$(this).animate({top: 50}, 1500, 'linear')})
	}else if (hard){
		$('#target').animate({top: 600}, 1000, 'linear', function(){$(this).animate({top: 50}, 1000, 'linear')})
	}
}

var velocityscroll = function(element){
    var mdown = false;
	var left = 0;
	var right = 0;
	var velocityvalue = 0;

    $(element).mousedown(function(e){
        e.preventDefault()
        mdown = true;
    })
    $(element).mousemove(function(e){
		if (mdown){
			e.preventDefault()
			left = getcss('#velocityscroll', 'left');
			right = left + getcss('#velocityscroll', 'width');
			if (left < 0){$('#velocityscroll').css('left','0px')}else if (right > 105){$('#velocityscroll').css('left','93px')}
			if ((left >= 0)&&(right <= 105)){
				$('#velocityscroll').css('left', (e.pageX - (9+getcss('#velocityscroll', 'width')/2))+'px')
				velocityvalue = left *(250/93);
				$('[name="velocity"]').val(velocityvalue.toFixed(0)+' m/s')
			}
		}
    })
    $('body').mouseup(function(e){
		if (left < 0){$('#velocityscroll').css('left','0px')}else if (right > 105){$('#velocityscroll').css('left','93px')}
        mdown = false;
		left = 0
		right = 0;
    })

}
var anglescroll = function(element){
    var mdown = false;
	var left = 0;
	var right = 0;
	var anglevalue = 0;

    $(element).mousedown(function(e){
        e.preventDefault()
        mdown = true;

    })
    $(element).mousemove(function(e){
		e.preventDefault()
		if (mdown){
			left = getcss('#anglescroll', 'left');
			right = left + getcss('#anglescroll', 'width');
			if (left < 0){$('#anglescroll').css('left','0px')}else if (right > 105){$('#anglescroll').css('left','93px')}
			if ((left >= 0)&&(right <= 105)){
				$('#anglescroll').css('left', (e.pageX - (121+getcss('#anglescroll', 'width')/2))+'px')
				anglevalue = left*(90/93);
				$('[name="angle"]').val(anglevalue.toFixed(0)+' Degrees')
			}
		}
    })
    $('body').mouseup(function(e){
		if (left < 0){$('#anglescroll').css('left','0px')}else if (right > 105){$('#anglescroll').css('left','93px')}
		mdown = false;

    })
}

var clickcolor = function(element, enter, down, leave){
    $(element).mouseenter(function(){
        $(this).stop().animate({backgroundColor: enter}, 200)
    })
    $(element).mousedown(function(){
        $(this).stop().animate({backgroundColor: down}, 100)
    })
    $(element).mouseup(function(){
        $(this).stop().animate({backgroundColor: enter}, 100)
    })
    $(element).mouseleave(function(){
        $(this).stop().animate({backgroundColor: leave}, 200)
	})
}

$(document).ready(function(){
	clickcolor('.button','#29CC00','#0B3800','#1F9C00');
	scroll('#velocityscroll', [0, parseInt($('.valueinputs').css('width'))+5], {horizontal: 'enabled', parameter: 'velocity'})
	scroll('#anglescroll', [0, parseInt($('.valueinputs').css('width'))+5], {horizontal: 'enabled', parameter: 'angle'})


	$('#lock').click(function(){
		angle = parseInt($('[name="angle"]').val())
		Vr = parseInt($('[name="velocity"]').val())
		Vy = (Math.sin(rad(angle)))*Vr;
		Vx = (Math.cos(rad(angle)))*Vr;
		totalDx = ((exp(Vr, 2))*(Math.sin(2*(rad(angle)))))/g;
		totalT = totalDx/Vx
		if ((Vr != 0) && (isNaN(Vr) == false) && (angle != 0) && (isNaN(angle) == false)){
			if ((easy == false) && (medium == false) && (hard == false)){
				alert('Please select the level.');
			}else{locked = true;$('#mechanism div:not(#locklaunch)').not('#launch').stop().animate({opacity: '-=0.3'}, 200); $('#disable').css('visibility', 'visible');if (balldraggable){$('#ball').draggable()}}
		}else{alert('Invalid Parameters')}
	})
	$('#launch').click(function(){
		if (locked){
			$(this).stop().animate({opacity: '-=0.3'}, 200).css('z-index', '50')
			targetmove();
			triggerball()
			$('#reset').stop().animate({opacity: 1, top: '+=10px'}, 200)
		}else{alert('Please lock the target first.')}
	})
	$('#reset').click(function(){
		location.reload()
	})
	$('#resultsinfo button').click()
	$('#levelcontainer div').click(function(){
		$(this).stop().animate({opacity: 1}, 200).siblings().animate({opacity: 0.5}, 200)
		switch($(this).attr('id')){
			case 'hard':
				hard = true
				easy = false;
				medium = false;
				balldraggable= false;
			break;
			case 'easy':
				easy = true
				medium = false;
				hard = false;
				balldraggable= true;
			break;
			case 'medium':
				medium = true;
				hard = false;
				easy= false;
				balldraggable= false;
			break;
		}
	})


	$('.scroll').mouseenter(function(){
		$(this).css('background', 'url("Data/scroll2.png") no-repeat')
	})
	$('.scroll').mousedown(function(){
		$(this).css('background', 'url("Data/scroll3.png") no-repeat')
	})
	$('.scroll').mouseup(function(){
		$(this).css('background', 'url("Data/scroll2.png") no-repeat')
	})
	$('.scroll').mouseleave(function(){
		$(this).css('background', 'url("Data/scroll1.png") no-repeat')
	})
})

var scroll = function(element, bounds, argument){
	if (argument['adoptcss'] == true){
		/**Css Code**/
		if (argument['vertical'] == 'enabled'){
			bounds[0] = parseInt($(element).css('margin-top'))
		}else if (argument['horizontal'] == 'enabled'){
			bounds[0] = parseInt($(element).css('margin-left'))
		}
	}
	var mdown = false;
	var mdownInitialX = 0;
	var mdownInitialY = 0;
	var elementInitialMarginX = 0;
	var elementInitialMarginY = 0;
	var elementMovingMargin = 0;
	var elementWidth = 0;
	var elementHeight = 0;
	$(element).mousedown(function(e){
		e.preventDefault();
		mdown = true;
		mdownInitialX = e.pageX;
		mdownInitialY = e.pageY;
		elementInitialMarginX = parseInt($(element).css('margin-left'));
		elementInitialMarginY = parseInt($(element).css('margin-top'));
		elementWidth = parseInt($(element).css('width'));
		elementHeight = parseInt($(element).css('height'));
	})
	$('body').mousemove(function(e){
		if (mdown){
			e.preventDefault()
			if (argument['vertical'] == 'enabled'){
				$(element).css('margin-top',elementInitialMarginY +e.pageY - mdownInitialY+'px');
				elementMovingMargin = parseInt($(element).css('margin-top'))
				if (elementMovingMargin < bounds[0]){
					$(element).css('margin-top', bounds[0]+'px');
				}else if (elementMovingMargin + elementHeight > bounds[1]){
						$(element).css('margin-top', bounds[1] - elementHeight+'px');
				}
			}else if (argument['horizontal'] == 'enabled'){
				$(element).css('margin-left',elementInitialMarginX +e.pageX - mdownInitialX+'px');
				elementMovingMargin = parseInt($(element).css('margin-left'))
				if ((elementMovingMargin > bounds[0])&&(elementMovingMargin + elementWidth < bounds[1])){
					if (argument['parameter'] == 'velocity'){
						velocityvalue = (elementMovingMargin + 8) *(250/bounds[1]);
						$('[name="velocity"]').val(velocityvalue.toFixed(0)+' m/s')
					}else if (argument['parameter'] == 'angle'){
						anglevalue = (elementMovingMargin + 8)*(90/bounds[1]);
						$('[name="angle"]').val(anglevalue.toFixed(0)+' Degrees')
					}
				}
				if (elementMovingMargin < bounds[0]){
					$(element).css('margin-left', bounds[0]+'px');
				}else if (elementMovingMargin + elementWidth > bounds[1]){
						$(element).css('margin-left', bounds[1] - elementWidth+'px');
				}
			}
		}
	})
	$('body').mouseup(function(e){
		if (mdown){
			e.preventDefault();
			mdown = false;
		}
	})
}
